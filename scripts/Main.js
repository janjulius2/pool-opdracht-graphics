var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

var cameraRotateSpeed = 5;

var renderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true
});

var movingballs = 0;

var loop = new GameLoop(120);

var table, cue;

var radius = 2.8;

var cueShooting = false;

var endGame = false;

var currentPlayer = 0;

var players = {
    0: new Player(),
    1: new Player()
};

var poolballs = [];
var ballTextures = [];

var startPosX = 0,
    startPosY = 0,
    startPosZ = -5;
var maxZoomIn = 2,
    maxZoomOut = 0.75;

animate();
//initiates the game / creates the game
function init() {
    var d = document.body.getElementsByClassName("wrapper");
	d[1].remove();
	d[0].remove();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    document.body.appendChild(renderer.domElement);

    createGUI();
    setTurn(currentPlayer);

    loop.add(function() {
        onLoop()
    });

    let incrZL = false;
    let incrXL = false;
    let incrZR = true;
	let incrXR = true;
	
    //key down input (camera, rotation camera, rotating cue)
    window.addEventListener('keydown', function(event) {
        if (event.keyCode == 37) { //left
            if (camera.position.z <= -18.0) {
                incrZL = true;
                incrZR = false;
            } else if (camera.position.z >= 18.0) {
                incrZL = false;
                incrZR = true;
            }
            if (camera.position.x <= -18.0) {
                incrXL = true;
                incrXR = false;
            }
            if (camera.position.x >= 18.0) {
                incrXL = false;
                incrXR = true;
            }
            if (incrXL) {
                camera.position.x += 0.1 * cameraRotateSpeed;
            } else {
                camera.position.x -= 0.1 * cameraRotateSpeed;
            }
            if (incrZL) {
                camera.position.z += 0.1 * cameraRotateSpeed;
            } else {
                camera.position.z -= 0.1 * cameraRotateSpeed;
            }
        } else if (event.keyCode == 39) { //right
            if (camera.position.z <= -18.0) {
                incrZR = true;
                incrZL = false;
            } else if (camera.position.z >= 18.0) {
                incrZR = false;
                incrZL = true;
            }
            if (camera.position.x <= -18.0) {
                incrXR = true;
                incrXL = false;
            }
            if (camera.position.x >= 18.0) {
                incrXR = false;
                incrXL = true;
            }
            if (incrXR) {
                camera.position.x += 0.1 * cameraRotateSpeed;
            } else {
                camera.position.x -= 0.1 * cameraRotateSpeed;
            }
            if (incrZR) {
                camera.position.z += 0.1 * cameraRotateSpeed;
            } else {
                camera.position.z -= 0.1 * cameraRotateSpeed;
            }
        }
        camera.lookAt(table.tableObject.position);

        if (event.keyCode == 65 && !players[currentPlayer].hasShot) { //a
            cue.moveCue('left');
        } else if (event.keyCode == 68 && !players[currentPlayer].hasShot) { //d
            cue.moveCue('right');
        } else if (event.keyCode == 87 && !players[currentPlayer].hasShot) { //w
            cue.moveCue('forward');
        } else if (event.keyCode == 83 && !players[currentPlayer].hasShot) { //s
            cue.moveCue('backward');
        } else if (event.keyCode == 32 && !cueShooting && !players[currentPlayer].hasShot) { //space
            console.log("space pressed");
            cueShooting = true;
            players[currentPlayer].hasShot = true;
        }
    });

    //zooming 
    window.addEventListener("wheel", function(e) {
        if (e.deltaY < 0 && camera.zoom < maxZoomIn) {
            camera.zoom += 0.075;
        }
        if (e.deltaY > 0 && camera.zoom > maxZoomOut) {
            camera.zoom -= 0.075;
        }
        camera.updateProjectionMatrix();
    }, true);

    table = new PoolTable(0x00FF00, 0x542606);
    table.LoadBalls();
    cue = new Cue(poolballs[0]);
    cue.moveCue();
    scene.add(cue.gameObject);

    camera.position.z = 15.0;
    camera.position.y = 18.0;
    camera.rotation.x = -4.5;

    addLights();
}

renderer.setClearColor(0x89cff0, 0);

//ads lights to the scene
function addLights() {
    var ambient = new THREE.AmbientLight(0xffffff, 0.1);
    scene.add(ambient);

    spotLight = new THREE.SpotLight(0xffffff, 1);
    spotLight.position.set(0, 6, 0);
    spotLight.penumbra = 0.05;
    spotLight.angle = 2;
    spotLight.decay = 2;
    spotLight.distance = 400;
    spotLight.castShadow = true;
    spotLight.shadow.mapSize.width = 2048;
    spotLight.shadow.mapSize.height = 2048;
    spotLight.shadow.camera.near = 1;
    spotLight.shadow.camera.far = 500;
    scene.add(spotLight);

    camera.lookAt(table.tableObject.position);
}

//animating loop
function animate() {
    if (cueShooting) {
        cue.shoot();
    }
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
}

//gameloop
function onLoop() {
    for (let i = 0; i < poolballs.length; i++)
        for (let j = i + 1; j < poolballs.length; j++)
            if (poolballs[i].colliding(poolballs[j]))
                poolballs[i].resolveCollision(poolballs[j]);
}

//time for next round
function nextRound() {
    if (!endGame) {
        players[currentPlayer].hasShot = false;
        if (!players[currentPlayer].canShootAgain) {
            currentPlayer = currentPlayer ? 0 : 1;
        }
        setTurn(currentPlayer);
        scene.add(cue.gameObject);
        cue.reset();
        cue.moveCue();
        players[currentPlayer].canShootAgain = false;

        console.log("players: " + (currentPlayer + 1) + " turn");
    }
}

//creates the player gui 
function createGUI() {
    var p = [];
    p1 = document.getElementById("player1Gui");
    p2 = document.getElementById("player2Gui");
    p.push(p1);
    p.push(p2);

    var n1 = p1.getElementsByClassName("playername");
    var n2 = p2.getElementsByClassName("playername");
    n1[0].innerHTML = "Player 1";
    n2[0].innerHTML = "Player 2";
    for (var i = 0; i < p.length; i++) {
        p[i].style.height = "400px";
        p[i].style.width = "200px";
        p[i].style.position = "absolute";
        p[i].style.backgroundColor = "black";
        p[i].style.border = "thick solid #0000FF"
    }
    p2.style.right = "0px";
    p1.style.float = "left";
}

//sets the player gui to the next players turn
function setTurn(id) {
    p1 = document.getElementById("player1Gui");
    p2 = document.getElementById("player2Gui");
    var x1 = p1.getElementsByClassName("ballsleft");
    var x2 = p2.getElementsByClassName("ballsleft");
    var t1 = p1.getElementsByClassName("balltype");
    var t2 = p2.getElementsByClassName("balltype");
    if (id == 0) {
        p1.style.border = "thick solid #00FF00";
        p2.style.border = "thick solid #ff0000";
    } else if (id == 1) {
        p2.style.border = "thick solid #00FF00";
        p1.style.border = "thick solid #ff0000";
    }
    console.log(x1);
    x1[0].innerHTML = "Balls left: " + players[0].ballsLeft;
    x2[0].innerHTML = "Balls left: " + players[1].ballsLeft;
    t1[0].innerHTML = "You are: " + ballText(players[0].playState);
    t2[0].innerHTML = "You are: " + ballText(players[1].playState);
}

//sets the winner for the player gui
function setWinner(id) {
    p1 = document.getElementById("player1Gui");
    p2 = document.getElementById("player2Gui");
    var e1 = p1.getElementsByClassName("end");
    var e2 = p2.getElementsByClassName("end");
    if (id == 0) {
        e1[0].innerHTML = "WINNER";
        e1[0].style.color = "#00FF00";
        p2.style.filter = "grayscale(100%)";
        p2.style.color = "#d3d3d3";
        p2.style.opacity = "0.2";
        e2[0].innerHTML = "LOSER";
        p1.style.border = "thick solid #00FF00";
    } else if (id == 1) {
        e2[0].innerHTML = "WINNER";
        e2[0].style.color = "#00FF00";
        p1.style.filter = "grayscale(100%)";
        p1.style.color = "#d3d3d3";
        p1.style.opacity = "0.2";
        e1[0].innerHTML = "LOSER";
        p2.style.border = "thick solid #00FF00";
    }
}

//returns text values for player gui
function ballText(a) {
    switch (a) {
        case 0:
            return "none";
        case 1:
            return "solid";
        case 2:
            return "striped";
    }
}