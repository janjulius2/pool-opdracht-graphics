//represents the pool cue 
class Cue {
    //constructor
    constructor(wb) {
        texture = new THREE.TextureLoader().load("image/poolq.jpg");
        var material, geometry;
        this.gotVar = false;
        this.startShot;
        this.endShot
        this.shootSpeed
        this.drawSpeed;
        this.doneDrawing = false;
        var texture;
        this.distanceFromWhiteBall = 0.1;
        this.whiteBall = poolballs[0];
        this.shootAngle = 270;
        
        this.length = 16;
        material = new THREE.MeshPhongMaterial({
            color: 0x654321
        });
        geometry = new THREE.CylinderGeometry(0.05, 0.2, 14.7, 14.7);


        this.gameObject = new THREE.Mesh(geometry, material);


        this.drawSpeed = 0.1;
        this.shootSpeed = 0.4;
        this.gameObject.rotation.z = 100 * Math.PI / 180;
        this.gameObject.position.y = 1.5;
    }
    //moves the cue in direction given input done by main
    moveCue(dir) {
        var x, z, cue, angle, maxFwdLength, maxBckwdLength;
        maxFwdLength = 15;
        maxBckwdLength = 21;


        if (dir == 'left') {
            this.shootAngle++;
            if (this.shootAngle === 360) {
                this.shootAngle = 0;
            }
            this.moveCue();
        } else if (dir == 'right') {
            this.shootAngle--;
            if (this.shootAngle <= 0) {
                this.shootAngle = 359;
            }
            this.moveCue();
        } else if (dir == 'backward') {
            this.length++;
            if (this.length >= maxBckwdLength) {
                this.length = maxBckwdLength;
            }
        } else if (dir == 'forward') {
            this.length--;
            if (this.length <= maxFwdLength) {
                this.length = maxFwdLength;
            }
        }
        angle = (this.shootAngle + 180 > 360) ? this.shootAngle - 180 : this.shootAngle + 180;

        cue = this.gameObject;
        cue.position.x = this.whiteBall.gameObject.position.x;
        cue.position.z = this.whiteBall.gameObject.position.z;

        x = (this.length / 2 + this.distanceFromWhiteBall) * Math.cos(angle * Math.PI / 180);
        z = (this.length / 2 + this.distanceFromWhiteBall) * Math.sin(angle * Math.PI / 180);

        cue.position.x += x;
        cue.position.z += z;

        cue.rotation.y = -angle * Math.PI / 180;
    }
    //shoot the ball (merely the animation)
    shoot() {
        var ballspeed = this.startShot - 14;
        if (!this.gotVar) {
            this.startShot = this.length;
            this.gotVar = true;
            this.endShot = this.startShot + 2;
        }
        if (!this.doneDrawing) {
            this.length += this.drawSpeed;
        }
        if (this.length >= this.endShot) {
            this.doneDrawing = true;
        }
        if (this.doneDrawing) {
            this.length -= this.shootSpeed * (this.endShot - this.startShot);
            if (this.length <= 14.8) {
                cueShooting = false;
                this.whiteBall.setSpeed(new THREE.Vector3((Math.cos(this.shootAngle * Math.PI / 180) * 0.05) * ballspeed, 0, (Math.sin(this.shootAngle * Math.PI / 180) * 0.05) * ballspeed));
                scene.remove(this.gameObject);
            }
        }
        this.moveCue();
    }
    //resets the pool cues variables
    reset() {
        this.shootAngle = 270
        this.gotVar = false;
        this.length = 16;
    }


}