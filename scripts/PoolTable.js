class PoolTable {

    constructor(TableColor, EdgesColor) {
        this.textureLoader = new THREE.TextureLoader();
        let clothMap = this.textureLoader.load('image/pool_table.png'),
            clothMaterial = new THREE.MeshPhongMaterial({
                color: EdgesColor,
                specular: 0x050505,
                shininess: 100
            });

        this.pockets = {
            0: {
                position: new THREE.Vector3(6.75, 0, 13.5),
                radius: 1
            },
            1: {
                position: new THREE.Vector3(-6.75, 0, 13.5),
                radius: 1
            },
            2: {
                position: new THREE.Vector3(-7.2, 0, 0),
                radius: 1
            },
            3: {
                position: new THREE.Vector3(-6.75, 0, -13.5),
                radius: 1
            },
            4: {
                position: new THREE.Vector3(6.75, 0, -13.5),
                radius: 1
            },
            5: {
                position: new THREE.Vector3(7.2, 0, 0),
                radius: 1
            }
        };


        this.sizez = 27;
        this.sizex = 13.5;
        this.tableGeometry = new THREE.BoxGeometry(this.sizex + 0.8, 1, this.sizez + 0.7);
        this.tableMaterial = new THREE.MeshPhongMaterial({
            map: clothMap,
            color: TableColor,
            specular: 0x050505,
            shininess: 1
        });

        this.tableObject = new THREE.Mesh(this.tableGeometry, this.tableMaterial);

        let tableWallShapes = [
            this.pointsToShape(
                new THREE.Vector2(6, -13.85),
                new THREE.Vector2(-6, -13.85),
                new THREE.Vector2(-5.625, -13.5),
                new THREE.Vector2(5.625, -13.5)
            ),
            this.pointsToShape(
                new THREE.Vector2(-6, 13.85),
                new THREE.Vector2(-5.625, 13.5),
                new THREE.Vector2(5.625, 13.5),
                new THREE.Vector2(6, 13.85)
            ),
            this.pointsToShape(
                new THREE.Vector2(7.1, 12.75),
                new THREE.Vector2(6.75, 12.5),
                new THREE.Vector2(6.75, 0.75),
                new THREE.Vector2(7.1, 0.7)
            ),
            this.pointsToShape(
                new THREE.Vector2(7.1, -12.75),
                new THREE.Vector2(7.1, -0.7),
                new THREE.Vector2(6.75, -0.75),
                new THREE.Vector2(6.75, -12.5)
            ),
            this.pointsToShape(
                new THREE.Vector2(-7.1, -12.75),
                new THREE.Vector2(-6.75, -12.5),
                new THREE.Vector2(-6.75, -0.75),
                new THREE.Vector2(-7.1, -0.7)
            ),
            this.pointsToShape(
                new THREE.Vector2(-7.1, 12.75),
                new THREE.Vector2(-7.1, 0.7),
                new THREE.Vector2(-6.75, 0.75),
                new THREE.Vector2(-6.75, 12.5)
            )
        ];

        this.gameObject = this.shapesToMesh(tableWallShapes, .5, clothMaterial);
        this.gameObject.rotateX(Math.PI / 2);
        this.gameObject.receiveShadow = true;
        this.gameObject.castShadow = true;
        this.tableObject.receiveShadow = true;
        this.tableObject.position.set(0, -1, 0);
        scene.add(this.gameObject);
        scene.add(this.tableObject);
    }

    //returns a shape with the given points
    pointsToShape(...points) {
        points.reverse();
        return new THREE.Shape(points);
    }
    //returns a mesh with the given shapes
    shapesToMesh(shapes, depth, material = new THREE.MeshStandardMaterial({
        color: 0xffffff
    })) {
        let extrudeSettings = {
                bevelEnabled: true,
                bevelSize: 0.05,
                bevelThickness: 0.05,
                steps: 1,
                amount: depth
            },
            geometry = new THREE.ExtrudeGeometry(shapes, extrudeSettings),
            mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(0, 0, 0);
        return mesh;
    }

    //spawns all the balls on the pooltable
    LoadBalls() {
        for (var i = 0; i < 16; i++) {
            var cur = new PoolBall(i, i >= 9);
            poolballs.push(cur);
            cur.gameObject.position.y = startPosY;
            cur.gameObject.position.x = startPosX;
            cur.gameObject.position.z = startPosZ;
            switch (i) {
                case 0:
                    console.log(cur.gameObject.position.x + " " + cur.gameObject.position.z);
                    cur.gameObject.position.z = 7;
                    break;
                case 1:
                    startPosZ += -0.56;
                    startPosX += 0.28;
                    break;
                case 2:
                    startPosX += -0.56;
                    break;
                case 3:
                    startPosX += -0.28;
                    startPosZ += -0.56;
                    break;
                case 4:
                case 5:
                case 7:
                case 8:
                case 9:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    startPosX += 0.56;
                    break;
                case 6:
                    startPosX += -((0.56 + 0.56 + 0.28));
                    startPosZ += -0.56;
                    break;
                case 10:
                    startPosX += -((0.56 * 3) + 0.28);
                    startPosZ += -0.56;

                default:
                    break;
            }
        }
    }
}