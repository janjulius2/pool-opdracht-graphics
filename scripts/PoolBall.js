class PoolBall {

    constructor(number, stripe) {
        console.log(number + " " + stripe);
        var textureLoader = new THREE.TextureLoader();
        var material, geometry;
        var texture;
        texture = new THREE.TextureLoader().load("image/Ball" + number + ".jpg");
        material = new THREE.MeshPhongMaterial({
            map: texture
        });
        geometry = new THREE.SphereGeometry(0.28, 32, 32);
        this.gameObject = new THREE.Mesh(geometry, material);
        this.gameObject.castShadow = true;
        this.stripe = stripe;
        this.radius = 0.28;
        this.mass = 1;
        this.restitution = {
            ball: 0.95,
            wall: 0.8
        };
        this.number = number;
        this.currentRotation = 0;

        this.randomOrientation();

        scene.add(this.gameObject);



        this.nextPosition = this.gameObject.position;
        this.speed = new THREE.Vector3();
        this.rollFriction = 0.6;
        this.getOtherBalls = () => poolballs.filter((ball) => ball !== this);
        this.otherBalls = false;
        this.stoppedRolling = function() {};

    }
    //sets the speed of the poolball
    setSpeed(speed) {
        this.otherBalls = this.getOtherBalls();

        let ball = this;
        this.speed = speed;
        this.nextPosition = this.gameObject.position.clone().addVectors(this.speed, this.gameObject.position);
        if (!this.ballLoop) {
            movingballs++;
            this.ballLoop = loop.add(function() {
                ball.moveBall();
            });
        }
    }
    //moves the poolball in the direction with the speed it has
    moveBall() {
        this.speed.multiplyScalar(1 - this.rollFriction / loop.tps);
        let stopThreshold = 0.001;
        if (this.speed.length() < stopThreshold) {
            this.speed.set(0, 0, 0);
            movingballs--;
            if (movingballs === 0)
                nextRound();
            this.ballLoop = loop.remove(this.ballLoop);
        } else {
            let circumference = this.radius,
                traversedDistance = this.speed.length(),
                addedAngle = traversedDistance / circumference,
                rollDirection = this.speed.clone().normalize(),
                rotateAxis = new THREE.Vector3(0, 1, 0);
            rollDirection.applyAxisAngle(rotateAxis, Math.PI / 2);

            this.currentRotation += addedAngle;
            let quaternion = new THREE.Quaternion().setFromAxisAngle(rollDirection, this.currentRotation);
            this.gameObject.setRotationFromQuaternion(quaternion);


            this.currentPosition = this.nextPosition.clone();

            let collision = this.willCollideWall(),
                direction = collision.direction,
                distance = collision.distance;

            if (direction) { //Wallhit
                let speedTowardsTarget = this.speed.clone().multiply(direction),
                    speedLength = speedTowardsTarget.length();
                if (speedLength > 0.05) {
                    let frequency = 2 * this.speed.length();
                    frequency = frequency > 0.6 ? 0.6 : frequency;
                    this.speed.multiplyScalar(this.restitution.wall);
                }
                if (distance - this.radius < 0)
                    this.currentPosition.add(direction.clone().multiplyScalar(distance - this.radius));

                direction.reflect(direction).normalize(); //normaal
                let speed = this.speed;

                let outgoingVector = ((d, n) => d.sub(n.multiplyScalar(d.dot(n) * 2))),
                    outgoing = outgoingVector(speed.clone(), direction);

                this.speed = outgoing.clone();
            }

            let scorePocket = false;
            for (let pocket in table.pockets) {
                if (this.gameObject.position.distanceTo(table.pockets[pocket].position) < table.pockets[pocket].radius) {
                    scorePocket = true;
                    break;
                }
            }

            if (scorePocket) {

                console.log('pocketed ', this.number, 'stripe: ' + this.stripe);
                let cplayer = currentPlayer;
                let oplayer = currentPlayer ? 0 : 1;
                if (this.number != 8) {
                    if (this.number != 0) {
                        movingballs--;
                        this.ballLoop = loop.remove(this.ballLoop);
                        scene.remove(this.gameObject);


                        if (players[cplayer].playState == 0) {
                            players[cplayer].playState = this.stripe ? 2 : 1;
                            players[oplayer].playState = this.stripe ? 1 : 2;
                            console.log(players[cplayer].playState);
                            players[cplayer].ballsLeft--;
                            players[cplayer].canShootAgain = true;
                        } else if (players[cplayer].playState == 1) {
                            if (!this.stripe) {
                                players[cplayer].ballsLeft--;
                                players[cplayer].canShootAgain = true;
                            } else {
                                players[cplayer].canShootAgain = false;
                                players[oplayer].ballsLeft--;
                            }
                        } else if (players[cplayer].playState == 2) {
                            if (this.stripe) {
                                players[cplayer].ballsLeft--;
                                players[cplayer].canShootAgain = true;
                            } else {
                                players[cplayer].canShootAgain = false;
                                players[oplayer].ballsLeft--;
                            }
                        }

                    } else {
                        this.gameObject.position.set(0, 0, 7);
                        this.setSpeed(new THREE.Vector3(0, 0, 0));
                    }
                } else {
                    movingballs--;
                    this.ballLoop = loop.remove(this.ballLoop);
                    scene.remove(this.gameObject);
                    if (players[cplayer].ballsLeft == 0) {
                        setWinner(cplayer);
                    } else {
                        setWinner(oplayer);
                    }
                }
            } else {

                this.gameObject.position.set(this.currentPosition.x, this.currentPosition.y, this.currentPosition.z);
                this.nextPosition = this.currentPosition.addVectors(this.speed, this.gameObject.position);
            }
        }
    }
    //checks if the poolball will collide with the wall the next tick
    willCollideWall() {
        if (table.sizex / 2 - Math.abs(this.nextPosition.x) > 4 && table.sizez / 2 - Math.abs(this.nextPosition.z) > 4) {
            return false;
        }
        if (this.nextPosition.x > 7.1)
            return {
                direction: new THREE.Vector3(1, 0, 0),
                distance: 6.7 - this.radius - this.gameObject.position.z
            };
        if (this.nextPosition.x < -7.1)
            return {
                direction: new THREE.Vector3(-1, 0, 0),
                distance: -6.7 + this.radius - this.gameObject.position.z
            };

        if (this.nextPosition.z > 13.5)
            return {
                direction: new THREE.Vector3(0, 0, 1),
                distance: 13.45 - this.radius - this.gameObject.position.z
            };
        if (this.nextPosition.z < -13.5)
            return {
                direction: new THREE.Vector3(0, 0, -1),
                distance: -13.45 + this.radius - this.gameObject.position.z
            };

        let pX = this.nextPosition.x > 0, //Bal zit in de positieve X helft
            pZ = this.nextPosition.z > 0; //Bal zit in de positieve Z helft
        let directions = [];
        pX && directions.push(new THREE.Vector3(1, 0, 0));
        pZ && directions.push(new THREE.Vector3(0, 0, 1));
        !pX && directions.push(new THREE.Vector3(-1, 0, 0));
        !pZ && directions.push(new THREE.Vector3(0, 0, -1));
        pZ && !pX && directions.push(new THREE.Vector3(-1, 0, 1));
        pZ && !pX && directions.push(new THREE.Vector3(-1, 0, 1));
        !pZ && pX && directions.push(new THREE.Vector3(1, 0, -1));
        !pZ && !pX && directions.push(new THREE.Vector3(-1, 0, -1));

        pX && pZ && directions.push(new THREE.Vector3(1, 0, 2));
        pZ && pX && directions.push(new THREE.Vector3(2, 0, 1));
        !pX && pZ && directions.push(new THREE.Vector3(-1, 0, 2));
        !pZ && pX && directions.push(new THREE.Vector3(2, 0, -1));
        pZ && !pX && directions.push(new THREE.Vector3(-2, 0, 1));
        pZ && !pX && directions.push(new THREE.Vector3(-2, 0, 1));
        !pZ && pX && directions.push(new THREE.Vector3(1, 0, -2));
        !pZ && !pX && directions.push(new THREE.Vector3(-1, 0, -2));

        let startPoint = this.nextPosition,
            ray = new THREE.Raycaster(startPoint),
            closestWall = Infinity,
            dir = false;
        for (let direction of directions) {
            ray.ray.direction = direction;
            let intersects = ray.intersectObjects([table.gameObject]);
            if (intersects.length > 0) {

                if (intersects[0].distance < closestWall) {
                    dir = direction;
                    closestWall = intersects[0].distance;
                }
            }
        }
        if (!dir || closestWall > this.radius)
            return false;
        return {
            direction: dir,
            distance: closestWall
        };
    }

    //checks if the poolball will colide with the given poolball
    colliding(ball) {
        let distance = this.nextPosition.distanceTo(ball.nextPosition);
        return distance < ball.radius + this.radius;
    }
    //resolves the collison with the other ball
    resolveCollision(ball) {

        let delta = this.gameObject.position.clone().sub(ball.gameObject.position),
            distance = delta.length();
        distance -= this.radius + ball.radius;
        if (distance < 0)
            this.gameObject.position.sub(delta.clone().normalize().multiplyScalar(distance));
        let dx = this.nextPosition.x - ball.nextPosition.x,
            dy = this.nextPosition.z - ball.nextPosition.z,
            collisionAngle = Math.atan2(dy, dx),
            speed1 = this.speed.length(),
            speed2 = ball.speed.length(),
            direction1 = Math.atan2(this.speed.z, this.speed.x),
            direction2 = Math.atan2(ball.speed.z, ball.speed.x),

            velocityx_1 = speed1 * Math.cos(direction1 - collisionAngle),
            velocityy_1 = speed1 * Math.sin(direction1 - collisionAngle),
            velocityx_2 = speed2 * Math.cos(direction2 - collisionAngle),
            velocityy_2 = speed2 * Math.sin(direction2 - collisionAngle),

            final_velocityx_1 = ((this.mass - ball.mass) * velocityx_1 + (ball.mass + ball.mass) * velocityx_2) / (this.mass + ball.mass),
            final_velocityx_2 = ((this.mass + this.mass) * velocityx_1 + (ball.mass - this.mass) * velocityx_2) / (this.mass + ball.mass),
            final_velocityy_1 = velocityy_1,
            final_velocityy_2 = velocityy_2;

        this.speed.x = Math.cos(collisionAngle) * final_velocityx_1 + Math.cos(collisionAngle + Math.PI / 2) * final_velocityy_1;
        this.speed.z = Math.sin(collisionAngle) * final_velocityx_1 + Math.sin(collisionAngle + Math.PI / 2) * final_velocityy_1;
        ball.speed.x = Math.cos(collisionAngle) * final_velocityx_2 + Math.cos(collisionAngle + Math.PI / 2) * final_velocityy_2;
        ball.speed.z = Math.sin(collisionAngle) * final_velocityx_2 + Math.sin(collisionAngle + Math.PI / 2) * final_velocityy_2;

        this.speed.multiplyScalar(this.restitution.ball);
        ball.speed.multiplyScalar(ball.restitution.ball);

        this.setSpeed(this.speed);
        ball.setSpeed(ball.speed);
    }
    //changes the rotation of the poolball to a random rotation
    randomOrientation() {
        this.gameObject.rotation.x = Math.floor((Math.random() * 360) + 1);
        this.gameObject.rotation.y = Math.floor((Math.random() * 360) + 1);
        this.gameObject.rotation.z = Math.floor((Math.random() * 360) + 1);

    }
}